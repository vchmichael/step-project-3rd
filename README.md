# Dan-IT Step projex - CARDS

DAN-IT 
Step Project Forkio
[Cards live version](https://vchmichael.gitlab.io/step-project-3/index.html)

Login - vchmichael@gmail.com
PW - devteam

***Studens***
-----------------------------------
- Michael Velychkevych 
- Maxim Ivanov 
- Nastya Korostilyova

***Technologies***
-----------------------------------
- Webpack 4
- HTML5
- SCSS
- JS
- Gitlab Pages

***Mykhailo Velychkevych***
-----------------------------------
Class Cards, Class Visit, Class App, GitLab Pages

***Maxim Ivanov***
-----------------------------------
Class Modal, Class Form

***Nastya Korostilyova***
-----------------------------------
Class Board, Class doctors(therapist, dantist, cardiologist)

***Team Work***
-----------------------------------
Search, filters, Git, Code Review, Design


