import * as interact from 'interactjs'
import {Modal} from './modal'

export class Card {
    constructor(visit) {
        this.btnDelete = document.createElement('button');
        this.btnEdit = document.createElement('button');
        this.btnMore = document.createElement('button');
        this.cardBody = document.createElement('div')
        this.visit = visit;
        this.id = visit.id
        this.cardsInfo = [];
    }

    render() {
        console.log(this.visit)
        this.cardBody.classList.add('desk__card')
        this.btnEdit.innerText = 'Edit'
        this.btnMore.innerText = 'Details'
        this.btnDelete.innerText = 'x'
        this.btnEdit.classList.add('btn', 'btn-card', 'btn-edit')
        this.btnMore.classList.add('btn', 'btn-card')
        this.btnDelete.classList.add('btn', 'btn-card', 'btn-delete')
        const title = document.createElement('h4')
        const doctor = document.createElement('span')
        const description = document.createElement('span')
        const fullName = document.createElement('span')
        const urgency = document.createElement('span')
        const visitReason = document.createElement('span')
        const status = document.createElement('span')
        status.innerText = `Status: ${this.visit.status}`
        doctor.innerText = `Doctor: ${this.visit.doctor}`
        title.innerText = this.visit.title
        description.innerText = `Description: ${this.visit.description}`
        fullName.innerText = `Full name: ${this.visit.fullname}`
        urgency.innerText = `Urgency: ${this.visit.urgency}`
        visitReason.innerText = `Visit reason: ${this.visit.visitreason}`
        this.cardsInfo.push(title, doctor, description, fullName, urgency, visitReason, status, this.btnMore, this.btnEdit, this.btnDelete);
        this.cardBody.append(...this.cardsInfo)
        document.querySelector('.desk__card-block').append(this.cardBody)
        this.btnDelete.addEventListener('click', this.deleteCard.bind(this))
        this.btnMore.addEventListener('click', () => {
            if (!document.querySelector('.doctorblock')) {
                this.showMore()
                this.btnMore.innerText = 'Hide'
            } else {
                this.btnMore.innerText = 'Details'
                document.querySelector('.doctorblock').remove()
            }

        })
        this.btnEdit.addEventListener('click', () => {
            this.editCard()
        })
        //START DRAG & DROP
        interact(this.cardBody)
            .draggable({
                inertia: true,
                modifiers: [
                    interact.modifiers.restrictRect({
                        restriction: 'parent',
                        endOnly: true
                    })
                ],
                autoScroll: true,
                listeners: {
                    move: dragMoveListener
                }
            })

        function dragMoveListener(event) {
            let target = event.target
            let x = (parseFloat(target.getAttribute('data-x')) || 0) + event.dx
            let y = (parseFloat(target.getAttribute('data-y')) || 0) + event.dy
            target.style.webkitTransform =
                target.style.transform =
                    'translate(' + x + 'px, ' + y + 'px)'
            target.setAttribute('data-x', x)
            target.setAttribute('data-y', y)
        }

        //END DRAG & DROP
    }

    showMore() {
        const doctorBlock = document.createElement('div')
        doctorBlock.classList.add('doctorblock')
        this.cardBody.append(doctorBlock)
        if (this.visit.doctor === 'Therapist') {
            const age = document.createElement('span');
            age.innerText = `Age: ${this.visit.age} years`
            doctorBlock.append(age);

        }
        if (this.visit.doctor === 'Dantist') {
            const date = document.createElement('span');
            date.innerText = `Date of last visit: ${this.visit.lastvisit}`
            doctorBlock.append(date);

        }
        if (this.visit.doctor === 'Сardiologist') {
            const age = document.createElement('span');
            const blood = document.createElement('span');
            const bodyIndex = document.createElement('span');
            const diseases = document.createElement('span');
            age.innerText = `Age: ${this.visit.age} years`
            blood.innerText = ` Blood presure: ${this.visit.bloodPresure}`
            bodyIndex.innerText = `Body index: ${this.visit.bodyIndex}`
            diseases.innerText = `Past diseases: ${this.visit.diseases}`
            doctorBlock.append(age, blood, bodyIndex, diseases);

        }
    }

    deleteCard() {
        fetch(`https://cards.danit.com.ua/cards/${this.id}`, {
            method: 'DELETE',
            headers: {
                'Authorization': `Bearer ${localStorage.getItem('token')}`
            },
        })
            .then((res) => {
                if (res.status >= 200 && res.status < 300) {
                    return res;
                } else {
                    let error = new Error(res.statusText);
                    error.response = res;
                    throw error
                }
            })
            .then(res => res.json())
            .then(res => {
                if (res.status === 'Success') {
                    this.cardBody.remove()
                    this.btnDelete.removeEventListener('click', this.deleteCard)
                    window.location.reload()
                }
            })
    }

    editCard() {
        if(!document.querySelector('.modal-login')){
            let editModal = new Modal().editCard(this.visit)
        }
        
    }
}